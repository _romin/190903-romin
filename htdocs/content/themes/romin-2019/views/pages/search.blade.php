@extends('layouts.main')

@section('content')
    <div class="content">
        <main class="content__main">
            <h2>{{ 'Search for: ' . get_search_query() }}</h2>
            @if (have_posts())
                <div class="articles">
                    @while(have_posts())
                    @php(the_post())
                        @template('parts.content', 'entry')
                    @endwhile
                </div>
            @else 
                <p>{{ 'Sorry, nothing matched your search. Please try again.' }}</p>
            @endif
        </main>
    </div>
@endsection