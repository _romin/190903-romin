@extends('layouts.main')

@section('content')
    @loop
        <div class="content">
                <main class="content__main">
                    <h2>{{ get_the_title() }}</h2>
                    <article class="article" itemscope itemtype="http://schema.org/BlogPosting">
                        <main class="article__main  article__main--full">
                            <div itemprop="articleBody">
                                {!! get_the_content() !!}
                            </div>
                        </main>
                    </article>
                </main>
            </div>

        {{-- @if(comments_open() || get_comments_number())
            @php(comments_template('/views/comments/template.php'))
        @endif --}}
    @endloop
@endsection