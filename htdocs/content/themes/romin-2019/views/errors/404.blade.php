@extends('layouts.main')

@section('content')
    <div class="content"> 
        <main class="content__main">
            <article>
                <header>
                    <h2>{{ 'Not Found' }}</h2>
                </header>
                    <p>{{ 'Nothing found for the requested page. Try a search instead?' }}</p>
            </article>
        </main>
    </div>
@endsection