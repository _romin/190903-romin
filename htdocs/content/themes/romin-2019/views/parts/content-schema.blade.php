@php 
    $avatar = get_avatar_url(get_the_author_meta('ID')) 
@endphp

<span itemprop="headline" class="hidden">{{ get_the_title() }}</span>
<span itemprop="author"  class="hidden">{{ get_the_author() }}</span> 
<link itemprop="mainEntityOfPage" href="{{ get_the_permalink() }}"/>
<span itemprop="description" class="hidden">{{ get_the_excerpt() }}</span>
<span itemprop="datePublished" content="{{ get_the_date('Y-m-d') }}" class="hidden"></span>
<span itemprop="dateModified" content="{{ get_the_modified_date('Y-m-d') }}" class="hidden"></span>

<div itemprop="publisher" itemscope itemtype="http://schema.org/Organization" class="hidden">
    <div itemprop="logo" itemscope itemtype="http://schema.org/ImageObject">
        <meta itemprop="url" content="{{ $avatar }}">
        <img src="{{ $avatar }}" alt="{{ get_the_author() }}'">
    </div>
    <span itemprop="name">{{ get_the_author() }}</span>
</div>

@if (get_the_post_thumbnail_url())
    <div itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
        <meta itemprop="height" content="100%">
        <meta itemprop="width" content="100%">
        <meta itemprop="url" content="{{ get_the_post_thumbnail_url() }}">
    </div>
@endif