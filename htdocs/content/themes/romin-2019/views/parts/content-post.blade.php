<article class="article" itemscope itemtype="http://schema.org/BlogPosting">
    <header class="article__header">
        <div class="article__wrapper">
            <div class="grid @if(get_the_post_thumbnail_url()): grid--center @endif">
                @if(get_the_post_thumbnail_url())
                    <div class="grid__col-2 grid__col-2--p-r-1">
                @else
                    <div class="grid__col-1 grid__col-1--p-l-1">
                @endif

                <h1 class="article__heading">
                    {{ get_the_title() }}
                </h1>
                <div class="article__excerpt">
                    {{ get_the_excerpt() }}
                </div>

                <small class="article__author">Posted By: {{ get_the_author() }} - {{ get_the_date() }}</small> 
                <div class="share share--mobile">
                    @template('parts.content-share')
                </div>
            </div> 

                @if(get_the_post_thumbnail_url())
                    <div class="grid__col-2 grid__col-2--p-lr-1">
                        @if(has_post_thumbnail())
                            <img class="article__thumbnail" src="{{ get_the_post_thumbnail_url() }}" alt="">
                        @endif
                    </div>
                @endif
            </div>
        </div>
    </header>

    <div class="content">
        <div class="content__main">
            <div class="article__main">
                <div class="share share--desktop">
                    @template('parts.content-share')
                </div>

                <div>
                    {!! get_the_content() !!}
                </div>

                @if(get_the_tags())
                    <div class="tags">
                        @foreach (get_the_tags() as $tag)
                            <a href="{{ bloginfo('url') }}/tag/{{ $tag->slug }}" class="tag">{{ $tag->name }}</a>
                        @endforeach
                    </div>
                @endif

                <p>
                    {{ edit_post_link() }}
                </p>
            </div>
        </div>
    </div>
    @template('parts.content-schema')
</article>