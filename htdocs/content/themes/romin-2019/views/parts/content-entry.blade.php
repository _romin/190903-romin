<article class="article" itemscope itemtype="http://schema.org/BlogPosting">
    <div class="article__content">
        <h2 class="article__heading">
            <a href="{{ get_the_permalink() }}">{{ get_the_title() }}</a>
        </h2>
        <div class="article__excerpt">
            {!! get_the_excerpt() !!}
        </div>
        <small class="article__author">Posted By: {{ get_the_author() }} - {{ get_the_date() }}</small> 
    </div>
    @if (get_the_post_thumbnail_url())
        <a href="{{ the_permalink() }}" class="article__thumbnail" style="background-image: url('{{ the_post_thumbnail_url() }}')"></a>
    @endif
    @template('parts.content-schema')
</article>