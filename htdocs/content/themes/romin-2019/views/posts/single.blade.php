@extends('layouts.main')

@section('content')
    @loop
        @template('parts.content', 'post')

        {{-- @if(comments_open() || get_comments_number())
            @php(comments_template('/views/comments/template.php'))
        @endif --}}
    @endloop
@endsection