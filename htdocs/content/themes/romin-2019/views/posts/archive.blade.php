@extends('layouts.main')

@section('content')

<div class="content">
        <main class="content__main">
            <h2>{{ single_term_title() }}</h2>
            @if(have_posts())
                <div class="articles">
                    @while(have_posts())
                    @php(the_post())
                        @template('parts.content', 'entry')
                    @endwhile
                </div>
            @else
                @template('parts.content', 'none')
            @endif
        </main>
    </div>
@endsection