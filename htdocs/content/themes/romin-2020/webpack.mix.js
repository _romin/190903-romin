let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */
mix.setPublicPath('dist');

mix.webpackConfig({
    module: {
        rules: [{
            test: /\.scss$/,
            loader: "import-glob-loader"
        },
    ]}
});

mix.js('assets/js/theme.js', 'dist/js/theme.min.js')
    .sass('assets/sass/app.scss', 'dist/css/app.css')
    
