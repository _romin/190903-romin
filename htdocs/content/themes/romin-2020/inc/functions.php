<?php
use Themosis\Support\Facades\Filter;
use Theme\Walkers\NavigationWalker;

/**
 * Enqueue Fontawesome
 */
Asset::add('font-awesome-free', '//use.fontawesome.com/releases/v5.8.1/css/all.css', [], '5.8.1', 'all')->to('front');

/**
 * Remove custom actions from the header
 */
Action::remove('wp_head', 'print_emoji_detection_script', 7);
Action::remove('wp_print_styles', 'print_emoji_styles');
Action::remove( 'admin_print_scripts', 'print_emoji_detection_script' );
Action::remove( 'admin_print_styles', 'print_emoji_styles' );


/**
 * Walker based header navigation.
 *
 * @param array $args
 * @return string|false|void
 */
function header_navigation($args)
{
    return wp_nav_menu([
        'theme_location' => $args['menu'],
        'items_wrap'=> '%3$s',
        'walker' => new NavigationWalker(
            [ $args['parent_menu_class'] ],
            [ $args['child_menu_class'] ]
        ),
        'container' => $args['container'],
        'container_class' => $args['container_class'],
    ]);
}

/**
 * Add ACF options page.
 */
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
}

/**
 * Save ACF json file.
 */
Filter::add('acf/settings/save_json', function ($paths) {
    return get_stylesheet_directory() . '/acf-json';
});

/**
 * Load ACF json file.
 */
Filter::add('acf/settings/load_json', function ($paths) {
    unset($paths[0]);
    $paths[] = get_stylesheet_directory() . '/acf-json';
    return $paths;
});

