import $ from 'jquery';

const hero = {

  props: {},

  /**
   * Type the heading.
   *
   * @param {string} heading
   * @param {number} i
   * @param {function} callback
   */
  typeHeading(heading, i, callback) {
    const element = this.props.heading;
    const wait = this.props.headingWait;

    if (i < (heading.length)) {
      $(`.${element}`).html(`${heading.substring(0, i + 1)} 
      <span class="hero__heading-caret" aria-hidden="true"></span>`);

      setTimeout(() => {
        this.typeHeading(heading, i + 1, callback);
      }, 100);
    } else if (typeof callback === 'function') {
      setTimeout(callback, wait);
    }
  },

  /**
   * Increment heading text.
   *
   * @param {number} i
   */
  fadeOverlayHeading(i) {
    const element = this.props.overlay;

    $(`.${element}`).removeClass(`${element}--show`);
    $(`.${element}`).addClass(`${element}--hide`);

    $(`.${element}`).html(this.props.overlays[i]);

    setTimeout(() => {
      $(`.${element}`).addClass(`${element}--show`);
    }, 100);
  },

  /**
   * If last heading in the array, then repeat
   * the animation.
   *
   * @param {number} i
   */
  loop(i) {
    if (i === this.props.headings.length - 1) {
      setTimeout(() => {
        this.start(0);
      }, this.props.loopWait);
    }
  },

  /**
   * Write the heading and overlay text.
   *
   * @param {*} i
   */
  start(i) {
    const heading = this.props.headings[i];

    if (this.props.repeat) {
      this.loop(i);
    }

    if (heading && i < heading.length) {
      this.typeHeading(heading, 0, () => {
        this.start(i + 1);
      });

      this.fadeOverlayHeading(i);
    }
  },

  /**
   * Initialise.
   *
   * @param {object} options
   */
  init(options) {
    this.props = options;

    setTimeout(() => {
      this.start(0);
    }, this.props.delayStart);
  },
};

export { hero };
