import $ from 'jquery';

/**
 * Change the header color foreach section
 * you scroll to.
 * 
 */
$.fn.changeHeaderColour = function changeHeaderColour() {

  $(this).each((i, element) => {
    const elementTop = $(element).offset().top;
    const elementBottom = elementTop + $(element).outerHeight();
    const viewportTop = $(window).scrollTop() + 55;

    if (elementBottom <= viewportTop) {
      $('.header').addClass('header--black');
    } else if (elementTop <= viewportTop) {
      $('.header').removeClass('header--black');
    }
  });
};

/**
 * Navigation smooth scroll.
 * 
 */
$.fn.scrollToSection = function scrollToSection() {

  $(this).click(function () {
    $('.menu-navigation').removeClass('menu-navigation--show');
    $('.hamburger__menu').removeClass('hamburger__menu--close');

    setTimeout(function () {
      let hash = window.location.hash.replace('#', '.');

      if (window.location.hash) {
        $('body, html').animate({
          scrollTop: $(hash).offset().top
        }, 1250);
      }

    }, 500);

    $('body, html').bind('mousedown DOMMouseScroll mousewheel keyup', function () {
      $('body, html').stop();
    });
  });
};

/**
 * Hide and show element in an offset rangevent.
 * 
 * @param {number} offset
 */
$.fn.fadeOutOffset = function fadeOutOffset(offset) {

  const className = $(this).attr('class').split(' ')[0];

  if ($('html, body').scrollTop() < offset) {
    $(this).addClass(`${className}--show`);
  } else {
    $(this).removeClass(`${className}--show`);
  }
};

/**
 * Toggle Menu.
 * 
 */
$.fn.toggleNavigation = function toggleNavigation() {

  $(this).on('click', () => {
    $('.hamburger__background').toggleClass('hamburger__background--show');
    $('.hamburger__menu').toggleClass('hamburger__menu--close');

    $('body').toggleClass('menu-navigation--reset-header');

    setTimeout(() => {
      $('.menu-navigation').toggleClass('menu-navigation--show');
    }, 250);
  });
};

/**
 * Set ios PWA Message only for Safari.
 * 
 */
export const pwaIOSDownload = () => {

  var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
    navigator.userAgent &&
    navigator.userAgent.indexOf('CriOS') == -1 &&
    navigator.userAgent.indexOf('FxiOS') == -1;

  const isIos = () => {
    const userAgent = window.navigator.userAgent.toLowerCase();
    return /iphone|ipad|ipod/.test(userAgent);
  }

  const isInStandaloneMode = () => ('standalone' in window.navigator) && (window.navigator.standalone);

  if (isIos() && !isInStandaloneMode() && isSafari) {
    $('.ios-pwa').addClass('ios-pwa--show');
    $('.ios-pwa__spacer').addClass('ios-pwa__spacer--show');
  }

  if ($.cookie('close-pwa') === 'active') {
    $('.ios-pwa').removeClass('ios-pwa--show');
    $('.ios-pwa__spacer').removeClass('ios-pwa__spacer--show');
  }

  $('.ios-pwa__close').on('click', () => {
    $('.ios-pwa').removeClass('ios-pwa--show');
    $('.ios-pwa__spacer').removeClass('ios-pwa__spacer--show');
    $.cookie('close-pwa', 'active', { expires: 1 });
  });

};

/**
 * Set splash page.
 * 
 */
export const loadSplashPage = () => {

  if($('.splash').length) {
    $('body').addClass('fixed');

    setTimeout(() => { 
      $('body').removeClass('fixed');
      $('.splash').remove();
    }, 4500);
  }
};

/**
 * Set cookies.
 * 
 */
export const setCookies = () => {

  if ($.cookie('accepted-cookies') !== 'active') {
    $('.cookies').show();
  }

  $('.cookies__button').on('click', function () {
    $.cookie('accepted-cookies', 'active', { expires: 1 });
    $('.cookies').fadeOut();
  });
};

/**
 * Custom Cursor.
 * 
 * @param {object} event
 */
export const setCursor = () => {

  $(document).on('scroll DOMMouseScroll mousewheel mousemove', (event) => {

    $('.cursor').css({ transform: `translate3d(${event.pageX - 20}px, ${event.pageY - 20}px, 0px)` });
    $('.cursor__child').css({ transform: `translate3d(${event.pageX + -2}px, ${event.pageY + -2}px, 0px)` });
  });
  
  $(document).on('click', () => {
  
    $('.cursor').addClass('cursor--click');
    
    setTimeout(() => {
      $('.cursor').removeClass('cursor--click');
    }, 100);
  });

};
