<!doctype html>
<html {!! get_language_attributes() !!}>
<head>
    <meta charset="{{ get_bloginfo('charset') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="shortcut icon" href="/content/themes/romin-2020/assets/images/favicon.png" type="image/x-icon">
    <link rel="icon" href="/content/themes/romin-2020/assets/images/favicon.png" type="image/x-icon">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-144209683-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-144209683-2');
    </script>
    <script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/444fb221a5077a0da36b0a594/96b1fd7a64112e47f083cf965.js");</script>

    <!-- Hotjar Tracking Code for https://romin.co.uk -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1525976,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

    <script data-ad-client="ca-pub-5182889263762161" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    @head

</head>
<body>

    <div class="cursor"></div>
    <div class="cursor__child"></div>
    
    @include('partials.navigation')
    @include('partials.ios')

    @yield('content')
    
    @footer

</body>
</html>
