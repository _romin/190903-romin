 <section class="section-newsletter">
     <div class="section-newsletter__cover" style="background-image: url({{ get_sub_field('newsletter_section_background_image') }})">
         <div class="section-newsletter__overlay"></div>
         <div class="[ information ] [ animate fadeInDown ]">
             <div class="rellax" data-rellax-xs-speed="0" data-rellax-mobile-speed="0" data-rellax-tablet-speed="0" data-rellax-desktop-speed="-1">
                 <h1 class="information__heading">
                     {{ get_sub_field('newsletter_section_heading') }}
                 </h1>
                 <div class="information__description">
                     {!! get_sub_field('newsletter_section_description') !!}
                 </div>
             </div>
         </div>

         <div class="section-newsletter__sign-up rellax" data-rellax-xs-speed="0" data-rellax-mobile-speed="0" data-rellax-tablet-speed="0" data-rellax-desktop-speed="-1">
             <div class="form">
                <?php //gravity_form( 2, false, true, true, '', true ); ?>
             </div>
         </div>
     </div>
 </section>
