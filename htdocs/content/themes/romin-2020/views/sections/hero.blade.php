<section class="[ section-hero ] [ black-section ]">
    <div class="section-hero__background" style="background-image: url({{ get_sub_field('hero_background_image') }})"></div>
    <div class="[ section-hero__overlay ] [ rellax ]" data-rellax-speed="-1"></div>

    <div class="container__fluid">

        <div class="[ section-hero__heading ] [ rellax ]" data-rellax-xs-speed="0" data-rellax-mobile-speed="0" data-rellax-tablet-speed="-1" data-rellax-desktop-speed="-1">
        </div>

        <div class="[ social ] [ animate ]" data-wow-delay="4s">
            @if( have_rows('social_media', 'option') )
                @while ( have_rows('social_media', 'option') ) @php(the_row())
                    @if(get_sub_field('platform') == 'facebook')
                        <a href="{{ get_sub_field('link') }}">
                            <img class="social__icon" src="{{ app('wp.theme')->getUrl('assets/images/social/facebook.svg') }}" alt="Facebook">
                        </a>
                    @endif
                    @if(get_sub_field('platform') == 'instagram')
                        <a href="{{ get_sub_field('link') }}">
                            <img class="social__icon" src="{{ app('wp.theme')->getUrl('assets/images/social/instagram.svg') }}" alt="Instagram">
                        </a>
                    @endif
                @endwhile
            @endif
        </div>

        <div class="[ scroll ] [ animate ]">
            <div class="scroll__name">
                Scroll Down
            </div>
            <span class="scroll__arrow"></span>
        </div>
    </div>
</section>
