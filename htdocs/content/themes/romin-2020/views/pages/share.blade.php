<a href="https://www.facebook.com/sharer/sharer.php?u={{ get_the_permalink() }}" class="share__platform share__platform--facebook">
    <div class="share__icon">
        <i class="fab fa-facebook-f"></i>
    </div>
    <div class="share__description">Share</div>
</a>

<a href="https://www.linkedin.com/shareArticle?mini=true&url={{ get_the_permalink() }}&title={{ get_the_title() }}&summary={{ get_the_excerpt() }}&source=" class="share__platform share__platform--linkedin">
    <div class="share__icon">
        <i class="fab fa-linkedin-in"></i>
    </div>
    <div class="share__description">Share</div>
</a>