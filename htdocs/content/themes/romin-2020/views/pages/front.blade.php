@extends('layouts.main')

@section('content')

    @include('partials.header')

    <div class="splash">
        <div class="splash__wrapper">
            <div class="splash__icon splash__icon--show" style="background-image: url({{ app('wp.theme')->getUrl('assets/images/logo-white.svg') }})"></div>
            <div class="splash__text">
                finecode
            </div>
        </div>
    </div>

    @include('flexible-content') 
    
    <div class="cookies">
        <div class="column">
            <div class="column__content">
                <div class="cookies__message">
                    We use cookies to ensure that we give you the best experience on our website.
                </div>
            </div>
            <div class="column__button">
            <span class="cookies__button">
                Accept & Close
            </span>
            </div>
        </div>
    </div>

@endsection
