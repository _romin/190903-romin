<div class="[ header ]">
    <div class="container__fluid">
        <div class="header__logo header__logo--show">
        </div>

        <div class="hamburger__wrapper">
            <div class="hamburger__menu">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
</div>
