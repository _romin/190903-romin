<div class="hamburger__background"></div>

<div class="menu-navigation">
    <div class="container__fluid">
        <div class="grid">
     
            <div class="grid__col-2">
                <ul class="contact">
                    <li class="contact__item contact__item--muted">Get in touch:</li>

                    @if(get_field('phone_number', 'option'))
                        <li class="contact__item">
                            <a href="tel:{{ get_field('phone_number', 'option') }}">Call</a>
                        </li>
                    @endif

                    @if(get_field('email_address', 'option'))
                        <li class="contact__item">
                            <a href="mailto:{{ get_field('email_address', 'option') }}">Email</a>
                        </li>
                    @endif

                    @if( have_rows('social_media', 'option') )
                        @while ( have_rows('social_media', 'option') ) @php(the_row())
                                @if(get_sub_field('platform') == 'facebook')
                                <li class="contact__item">
                                    <a href="{{ get_sub_field('link') }}">Facebook</a>
                                </li>
                            @endif
                            @if(get_sub_field('platform') == 'instagram')
                                <li class="contact__item">
                                    <a href="{{ get_sub_field('link') }}">Instagram</a>
                                </li>
                            @endif
                        @endwhile
                    @endif
                </ul>
            </div>

            <div class="grid__col-2">
                <?php header_navigation([
                        'menu' => 'header-menu',
                        'container' => 'div',
                        'container_class' => 'menu',
                        'parent_menu_class' => 'menu__item',
                        'child_menu_class' => 'menu__sub-menu',
                ]); ?>
            </div>

        </div>
    </div>
</div>