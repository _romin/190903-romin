<?php

namespace Theme\Walkers;

use Walker_Nav_Menu;

class NavigationWalker extends Walker_Nav_Menu
{
    protected $navClass = array();
    protected $subMenuClass = array();

    /**
     * Construct nav and sub nav classes.
     *
     * @param array $navClass
     * @param array $subMenuClass
     */
    public function __construct($navClass, $subMenuClass)
    {
        $this->navClass = $navClass;
        $this->subMenuClass = $subMenuClass;
    }
    
    /**
     * Render each list item.
     *
     * @param string $output
     * @param object $item
     * @param integer $depth
     * @param array $args
     * @param integer $id
     * @return string
     */
    public function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    {
        $args = (object) $args;

        $link = '';
        $indent = ($depth) ? str_repeat("\t", $depth) : '';

        $classes = $this->navClass;
        $classes = join(
            ' ',
            apply_filters('nav_menu_css_class', $classes, $args, $depth)
        );

        $output .= $indent . '<li>';
        $attributes = $this->attributes([
            'title' => $item->attr_title,
            'target' => $item->target,
            'href' => $item->url,
            'rel' => $item->xfn,
        ]);

        $link .= '<a class='. $classes . $attributes .' data-hover="' . $item->title . '">';
        $link .= ($depth == 0 && $args->walker->has_children) ? '</a>' : '</a>';

        $output .= apply_filters('walker_nav_menu_start_el', $link, $item, $depth, $args);
    }

    /**
     * Set sub menu container class.
     *
     * @param string $output
     * @param integer $depth
     * @param array $args
     * @return string
     */
    public function start_lvl(&$output, $depth = 0, $args = array())
    {
        $link = '';
        $classes = $this->subMenuClass;

        $classes = join(
            ' ',
            apply_filters('nav_menu_submenu_css_class', $classes, $args, $depth)
        );

        $output .= "<ul class=".$classes.">{$link}";
    }

    /**
     * Set attributes.
     *
     * @param  array $attribute
     * @return string
     */
    public function attributes($attributes)
    {
        $string = '';

        foreach ($attributes as $attribute => $value) {
            $string .= ($value) ? ' ' . $attribute . '="'  . esc_attr($value) .'"' : '';
        }

        return $string;
    }
}
