<?php
/**
 * Application routes.
 */

Route::any('front', function() {
    return view('pages.front');
});

Route::get('page', function (\WP_Post $page) {
    $template = get_post_meta( $page->ID, '_wp_page_template', true);

    $page = get_query_var('page');
    
    $posts = new WP_Query([
        'post_status' => 'publish',
        'post_type' => 'post',
        'paged' => ($page) ? $page : 1,
        'posts_per_page' => 10,
        'orderby' => 'ID',
        'order' => 'DESC',
    ]);
    
    return view('templates.' . $template, compact('page', 'posts'));
});

Route::get('single', function (\WP_Post $post) {
    return view('posts.single');
});

Route::get('archive', function () {

    $page = get_query_var('page');
    
    $posts = new WP_Query([
        'post_status' => 'publish',
        'post_type' => 'post',
        'paged' => ($page) ? $page : 1,
        'posts_per_page' => 10,
        'orderby' => 'ID',
        'order' => 'DESC',
    ]);
    

    return view('posts.archive', compact('posts'));
});

Route::fallback(function () {
    return view('errors.404');
});